package org.openqa.selenium.example;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

//location.href = url;
public class RDOTest {
	private WebDriver driver;
	private Map<String, Object> vars;
	JavascriptExecutor js;
	String[] ming = { "admin.refdata.list.do?dataObjectKey=object.adhocSearchDefaultSettings",
			"admin.refdata.list.do?dataObjectKey=object.adhocSearchPermissions",
			"admin.refdata.list.do?dataObjectKey=object.adhocSearchSavedState",
			"admin.refdata.list.do?dataObjectKey=object.adrResultType",
			"admin.refdata.list.do?dataObjectKey=object.adrSourceOfNeutralType",
			"admin.refdata.list.do?dataObjectKey=object.agpIssueType",
			"admin.refdata.list.do?dataObjectKey=object.agpRequestedRemedyType",
			"admin.refdata.list.do?dataObjectKey=object.agpStepResultType",
			"admin.refdata.list.do?dataObjectKey=object.agpStepType",
			"admin.refdata.list.do?dataObjectKey=object.ahrAgreementType",
			"admin.refdata.list.do?dataObjectKey=object.ahrMemorandumType",
			"admin.refdata.list.do?dataObjectKey=object.ahrNaCaseResolutionType",
			"admin.refdata.list.do?dataObjectKey=object.ahrNaContentType",
			"admin.refdata.list.do?dataObjectKey=object.ahrNaDecisionOutcomeType",
			"admin.refdata.list.do?dataObjectKey=object.arbitrationComplianceRemedyGrantedType",
			"admin.refdata.list.do?dataObjectKey=object.arbitrationCpmsActionType",
			"admin.refdata.list.do?dataObjectKey=object.arbitrationDecisionClarificationFiledByType",
			"admin.refdata.list.do?dataObjectKey=object.arbitrationDecisionResultsType",
			"admin.refdata.list.do?dataObjectKey=object.arbitrationFilingPartyType",
			"admin.refdata.list.do?dataObjectKey=object.arbitrationRemandDecisionType",
			"admin.refdata.list.do?dataObjectKey=object.arbitrationReviewBasisType",
			"admin.refdata.list.do?dataObjectKey=object.arbitrationReviewDecisionType",
			"admin.refdata.list.do?dataObjectKey=object.arbRequestedRemedyType",
			"admin.refdata.list.do?dataObjectKey=object.audit_ref1",
			"admin.refdata.list.do?dataObjectKey=object.cadroResultType",
			"admin.refdata.list.do?dataObjectKey=object.cadroType",
			"admin.refdata.list.do?dataObjectKey=object.caseNumberCounter",
			"admin.refdata.list.do?dataObjectKey=object.caseType",
			"admin.refdata.list.do?dataObjectKey=object.changeActionType",
			"admin.refdata.list.do?dataObjectKey=object.componentInformation",
			"admin.refdata.list.do?dataObjectKey=object.contactAppointmentType",
			"admin.refdata.list.do?dataObjectKey=object.contactGradeType",
			"admin.refdata.list.do?dataObjectKey=object.contactPayPlanType",
			"admin.refdata.list.do?dataObjectKey=object.contactPrefixType",
			"admin.refdata.list.do?dataObjectKey=object.contactSuffixType",
			"admin.refdata.list.do?dataObjectKey=object.contactType",
			"admin.refdata.list.do?dataObjectKey=object.countryCallingCodeType",
			"admin.refdata.list.do?dataObjectKey=object.countryRegionType",
			"admin.refdata.list.do?dataObjectKey=object.countryType",
			"admin.refdata.list.do?dataObjectKey=object.disciplineAdrType",
			"admin.refdata.list.do?dataObjectKey=object.documentType",
			"admin.refdata.list.do?dataObjectKey=object.dueDateSettings",
			"admin.refdata.list.do?dataObjectKey=object.emailNotifications",
			"admin.refdata.list.do?dataObjectKey=object.formInstructionType",
			"admin.refdata.list.do?dataObjectKey=object.informationRequestRedressType",
			"admin.refdata.list.do?dataObjectKey=object.informationRequestTopicType",
			"admin.refdata.list.do?dataObjectKey=object.merActionType",
			"admin.refdata.list.do?dataObjectKey=object.merBasisForActionType",
			"admin.refdata.list.do?dataObjectKey=object.merBreachOfLcaNatureType",
			"admin.refdata.list.do?dataObjectKey=object.merDecisionActionType",
			"admin.refdata.list.do?dataObjectKey=object.merFurloughAppealType",
			"admin.refdata.list.do?dataObjectKey=object.merFurloughReasonType",
			"admin.refdata.list.do?dataObjectKey=object.merProposalResponseType",
			"admin.refdata.list.do?dataObjectKey=object.merSelectedRedressType",
			"admin.refdata.list.do?dataObjectKey=object.mspbAppealType",
			"admin.refdata.list.do?dataObjectKey=object.mspbHearingInitialDecisionType",
			"admin.refdata.list.do?dataObjectKey=object.mspbHearingMotionDecisionType",
			"admin.refdata.list.do?dataObjectKey=object.mspbHearingMotionFiledType",
			"admin.refdata.list.do?dataObjectKey=object.mspbHearingPfrDecisionType",
			"admin.refdata.list.do?dataObjectKey=object.mspbHearingRevisedDecisionType",
			"admin.refdata.list.do?dataObjectKey=object.naDecisionCaseResolutionType",
			"admin.refdata.list.do?dataObjectKey=object.naDecisionOutcomeType",
			"admin.refdata.list.do?dataObjectKey=object.negotiatedGrievanceProcedureIssueType",
			"admin.refdata.list.do?dataObjectKey=object.negotiatedGrievanceProcedureS1DecisionType",
			"admin.refdata.list.do?dataObjectKey=object.negotiatedGrievanceProcedureStepType",
			"admin.refdata.list.do?dataObjectKey=object.ngpFilingPartyType",
			"admin.refdata.list.do?dataObjectKey=object.ngpRequestedRemedyType",
			"admin.refdata.list.do?dataObjectKey=object.openclosedType",
			"admin.refdata.list.do?dataObjectKey=object.operatorType",
			"admin.refdata.list.do?dataObjectKey=object.parameterArgument",
			"admin.refdata.list.do?dataObjectKey=object.parameterNameType",
			"admin.refdata.list.do?dataObjectKey=object.parameterType",
			"admin.refdata.list.do?dataObjectKey=object.perCaseProcessingActionType",
			"admin.refdata.list.do?dataObjectKey=object.perCaseProcessingPipAssistanceType",
			"admin.refdata.list.do?dataObjectKey=object.perCaseProcessingPipResultsType",
			"admin.refdata.list.do?dataObjectKey=object.perDecisionDecidedActionType",
			"admin.refdata.list.do?dataObjectKey=object.perPerformanceRatingType",
			"admin.refdata.list.do?dataObjectKey=object.perProposalProposedActionType",
			"admin.refdata.list.do?dataObjectKey=object.perProposalResponseType",
			"admin.refdata.list.do?dataObjectKey=object.perRedressType",
			"admin.refdata.list.do?dataObjectKey=object.repElectionOutcomeType",
			"admin.refdata.list.do?dataObjectKey=object.repHearingDecisionOrderType",
			"admin.refdata.list.do?dataObjectKey=object.repIntervensionsFlraReviewDecisionType",
			"admin.refdata.list.do?dataObjectKey=object.repIntervensionsResolutionType",
			"admin.refdata.list.do?dataObjectKey=object.repPetitionBarsType",
			"admin.refdata.list.do?dataObjectKey=object.repPetitionType",
			"admin.refdata.list.do?dataObjectKey=object.roleLevel",
			"admin.refdata.list.do?dataObjectKey=object.sadCaseResolutionDecisionType",
			"admin.refdata.list.do?dataObjectKey=object.sadCaseResolutionOpmDecisionType",
			"admin.refdata.list.do?dataObjectKey=object.sadCaseResolutionResponseType",
			"admin.refdata.list.do?dataObjectKey=object.sadInvestigationProcessIssuesType",
			"admin.refdata.list.do?dataObjectKey=object.seriesType",
			"admin.refdata.list.do?dataObjectKey=object.standardReports",
			"admin.refdata.list.do?dataObjectKey=object.statusType",
			"admin.refdata.list.do?dataObjectKey=object.systemTemplates",
			"admin.refdata.list.do?dataObjectKey=object.systemType",
			"admin.refdata.list.do?dataObjectKey=object.ulpDecisionCourtDecisionType",
			"admin.refdata.list.do?dataObjectKey=object.ulpDecisionFlraDecisionType",
			"admin.refdata.list.do?dataObjectKey=object.ulpDecisionRemedyType",
			"admin.refdata.list.do?dataObjectKey=object.ulpDecisionResolutionType",
			"admin.refdata.list.do?dataObjectKey=object.ulpFilingAllegedViolationType",
			"admin.refdata.list.do?dataObjectKey=object.ulpFilingPartyType",
			"admin.refdata.list.do?dataObjectKey=object.unionType",
			"admin.refdata.list.do?dataObjectKey=object.userRegistration",
			"admin.refdata.list.do?dataObjectKey=object.verificationDataRequired",
			"admin.refdata.list.do?dataObjectKey=object.verificationDateEntry",
			"admin.refdata.list.do?dataObjectKey=object.yesnoLookupType" };

	@Before
	public void setUp() {
		driver = new ChromeDriver();
		js = (JavascriptExecutor) driver;
		vars = new HashMap<String, Object>();
	}

	@After
	public void tearDown() {
		driver.quit();
	}

	@Test
	public void countrdos() throws InterruptedException {
		// Test name: countrdos
		// Step # | name | target | value | comment
		// 1 | open | /chimera/login.request.do?service=%2Fhome.do | |
		String baseUrl = "http://localhost:8080/chimera/";
		driver.get(baseUrl);
		// driver.get("http://localhost:8080/chimera/login.request.do?service=%2Fhome.do");
		// 2 | setWindowSize | 1492x731 | |
		// driver.manage().window().setSize(new Dimension(1492, 731));
		// 3 | type | id=username | administrator |
		driver.findElement(By.id("username")).sendKeys("administrator");
		// 4 | type | id=password | admin1953 |
		driver.findElement(By.id("password")).sendKeys("admin1953");
		// 5 | sendKeys | id=password | ${KEY_ENTER} |
		driver.findElement(By.id("password")).sendKeys(Keys.ENTER);

		Thread.sleep(5000);
		// Let the user actually see something!	;

		System.out.println("Page title is: " + driver.getTitle());
		for (int i = 0; i < ming.length; i++) { 
			if (i> 3) break;
			String string = baseUrl + ming[i];
			System.out.println("Your browser URL will be " + string);			
			driver.get(string);
			Thread.sleep(5432);
			//String currentUrl = driver.getCurrentUrl();
			//System.out.println(currentUrl + "	Page title is: " + driver.getTitle());
		}
	}
}
